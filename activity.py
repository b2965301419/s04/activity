from abc import ABC, abstractmethod

class Animal(ABC):
	@abstractmethod
	def eat(self):
		pass
	def make_sound(self):
		pass

class Dog(Animal):
	def __init__(self, name, breed, age, food_given):
		super().__init__()
		self._name = name
		self._breed = breed
		self._age = age
		self._food_given = food_given

	## Methods
	
	# Getters
	def get_name(self):
		print(f'The name of the animal is {self._name}')
	def get_breed(self):
		print(f'The breed of the animal is {self._breed}')
	def get_age(self):
		print(f'The age of the animal is {self._age}')
	def get_food_given(self):
		print(f'The food that animal has been given is {self._food_given}')

	# Setters
	def set_name(self, name, breed, age, food_given):
		self._name = name
	def set_breed(self):
		self._breed = breed
	def set_age(self):
		self._age = age
	def set_food_given(self):
		self._food_given = food_given

	# Implementation of abstract methods
	def eat(self):
		print(f'Eaten {self._food_given}')
	def make_sound(self):
		print('Bark! Woof! Arf!')
	
	# Call()
	def call(self):
		print(f'Here {self._name}!')

class Cat(Animal):
	def __init__(self, name, breed, age, food_given):
		super().__init__()
		self._name = name
		self._breed = breed
		self._age = age
		self._food_given = food_given

	## Methods
	
	# Getters
	def get_name(self):
		print(f'The name of the animal is {self._name}')
	def get_breed(self):
		print(f'The breed of the animal is {self._breed}')
	def get_age(self):
		print(f'The age of the animal is {self._age}')
	def get_food_given(self):
		print(f'The food that animal has been given is {self._food_given}')

	# Setters
	def set_name(self, name, breed, age, food_given):
		self._name = name
	def set_breed(self):
		self._breed = breed
	def set_age(self):
		self._age = age
	def set_food_given(self):
		self._food_given = food_given

	# Implementation of abstract methods
	def eat(self):
		if self._food_given == 'Tuna':
			print('Ok')
		else:
			print('Serve me Tuna')
	def make_sound(self):
		print('Miaow! Nyaw! Nyaaaaa!')
	
	# Call()
	def call(self):
		print(f'{self._name}, come on!')

dog1 = Dog('Isis', 'Serbian Husky', 10, 'Steak')
cat1 = Cat('Puss', 'Birman', 9, 'Steak')

for animal in (dog1, cat1):
	animal.eat()
	animal.make_sound()
	animal.call()